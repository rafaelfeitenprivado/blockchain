# blockchain

Trabalho da cadeira de Aplicações Industriais.

Aplicação em nodeJS que realizar uma cadeira de blockchain sobre Heranças.

# TO RUN

Instalar os pacotes:

`npm i` | `yarn`


Para rodar o projeto em SO Windows, modificar o arquivo package.json colocando `SET` antes das variáveis;
Linux e Mac NOTHING (great job)

Rode o comando abaixo para parar todos processos node e evitar conflitos:

`killall node`


Rodar os comandos em janelas diferentes dentro da pasta blockchain: 

`npm run 1`

`npm run 2`

`npm run 3`

`npm run 4`


OBS: Peer 1 é apenas para minerar, não deve ser mandado blocos a partir dele.